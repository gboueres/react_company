  
import React from 'react'

const ItemPortfolio = props => {
    return (
        <div className='col-sm-4'>
            <div className='thumbnail'>
                <img src={props.conteudo.img} alt='' width='400' height='300' />
                <p><strong>{props.conteudo.title}</strong></p>
                <p>{props.conteudo.description}</p>
            </div>
        </div>
    )
}

export default ItemPortfolio