import React, {Component} from 'react'
import config, {storage} from './../firebase-config'

class AdminPortfolio extends Component {
    constructor (props) {
        super (props)

        this.gravaPortfolio = this.gravaPortfolio.bind(this)
    }

    gravaPortfolio(e) {
        const itemPortfolio = {
            title: this.title.value,
            description: this.description.value,
            img: this.img
        }
        this.setState({estaGravando: true})
        const arquivo = itemPortfolio.img.files[0]
        const {name, size, type} = arquivo

       const ref = storage.ref(name)
       ref.put(arquivo)
        .then(img => {
            img.ref.getDownloadURL()
                .then(downloadURL => {
                    const newPortfolio = {
                        title: itemPortfolio.title,
                        description: itemPortfolio.description,
                        img: downloadURL
                    }
                    config.push('portfolio', {
                        data: newPortfolio
                    })
                    this.setState({estaGravando: false})
                })
        })

        e.preventDefault()
    }


    render (){
        return (
            <div style={{padding: '120px'}}>
            <h2>Portfolio - Admin Area</h2>
            <form onSubmit={this.gravaPortfolio}>
                <div className="form-group">
                    <label htmlFor="tile">Títle</label>
                    <input type="text" className="form-control" id="titulo" placeholder="Title" ref={(ref) => this.title = ref} />
                </div>
                <div className="form-group">
                    <label htmlFor="description">Description</label>
                    <textarea className="form-control" id="description" rows="3" ref={(ref) => this.description = ref}></textarea>
                </div>
                <div className="form-group">
                    <label htmlFor="imagem">Img</label>
                    <input type="file" className="form-control-file" id="imagem" ref={(ref) => this.img = ref}/>
                </div>
                <button type="submit" className="btn btn-primary">Save</button>
            </form>
        </div>
        )
    }
}

export default AdminPortfolio